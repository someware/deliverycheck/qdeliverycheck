from enum import Enum

class Profiles(Enum):
    SOCLE_COMMUN_DXF = "Format DXF"
    SDEF_SHP = "SDEF (SHP)"
    LORIENT_AGGLO_DXF = "Lorient agglomération (DXF)"
    RENNES_METRO_DXF = "Rennes métropole (DXF)"

