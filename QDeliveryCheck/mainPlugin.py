import os
import sys

from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QMessageBox, QAction, QFileDialog, QToolButton, QMenu

from . import resources_rc

from .profiles import Profiles
from .utils import external_libs


class Plugin:

    def __init__(self, iface):
        self.iface = iface
        self.toolButton = QToolButton()
        self.toolButton.setMenu(QMenu())
        self.toolButton.setPopupMode(QToolButton.MenuButtonPopup)
        self.icon = QIcon(":/plugins/QDeliveryCheck/icon.png")
        self.menu = QMenu("QDeliveryCheck")
        self.menu.setIcon(self.icon)


    def initGui(self):
        choose_profile_action = self.make_action()
        from_config_file_action = QAction(
            "Charger une configuration depuis un fichier...",
            self.iface.mainWindow()
        )
        from_config_file_action.triggered.connect(self.load_from_file)
        about_action = QAction(
            "À propos de QDeliveryCheck...",
            self.iface.mainWindow()
        )
        about_action.triggered.connect(self.about)

        # create action that will start plugin configuration
        default_action = self.make_action(prefix="QDeliveryCheck : ")
        default_action.setIcon(self.icon)
        self.actions = [
            choose_profile_action,
            from_config_file_action,
        ] + [
            self.make_action(p) for p in Profiles
        ] + [
            about_action
        ]
        self.toolButton.setDefaultAction(default_action)
        self.toolButton.setToolTip("QDeliveryCheck")
        self.toolBtnAction = self.iface.addToolBarWidget(self.toolButton)
        for action in self.actions:
            self.menu.addAction(action)
            self.toolButton.menu().addAction(action)
        self.iface.pluginMenu().addMenu(self.menu)

    def unload(self):
        # remove the plugin menu item and icon
        for a in self.actions:
            self.iface.removePluginMenu("QDeliveryCheck", a)
        self.iface.removeToolBarIcon(self.toolBtnAction)

    def make_action(self, profile=None, prefix=''):
        action = QAction(
            '%s%s' % (prefix, "Choisir un profil de validation..." if profile is None else profile.value),
            self.iface.mainWindow()
        )
        action.triggered.connect(self.make_action_trigger(profile))
        return action

    def run_wizard(self, profile=None, config_file=None):
        with external_libs():
            from .gui.wizard import PluginWizard
            self.page = PluginWizard(self.iface, profile, config_file)
            self.page.setGeometry(0, 0, 600, 600)
            self.page.show()

    def about(self):
        infoString = """
        <h2>Plugin QDeliveryCheck</h2>
        <p>
          Un plugin pour QGIS permettant le contrôle de données topographiques
          et l'export de celles-ci au format PCRS (GML).
        </p>
        <p>
          Voir la documentation en ligne :
          <a href="https://someware.gitlab.io/deliverycheck/qdeliverycheck/">someware.gitlab.io/deliverycheck/qdeliverycheck/</a>.
        </p>
        <p>Développé par <a href="https://www.someware.fr">Someware</a>.</p>
        <p>Développement initial financé par :
            <ul>
                <li>Région Bretagne</li>
                <li>Région Pays de Loire</li>
                <li>Lorient Agglomération</li>
                <li>Rennes Métropole</li>
                <li>Brest Métropole</li>
                <li>Lannion-Trégor Communauté</li>
                <li>SDEF</li>
                <li>SDE 22</li>
                <li>SDE 35</li>
                <li>Morbihan Énergies</li>
                <li>Redon Agglomération</li>
                <li>Quimperlé Communauté</li>
                <li>Ploërmel Communauté</li>
                <li>Communauté de Communes du Pays Fouesnantais</li>
            </ul>
        </p>
        """
        QMessageBox.information(
            self.iface.mainWindow(),
            "À propos de QDeliveryCheck",
            infoString
        )


    def make_action_trigger(self, profile=None):
        def action():
            self.run_wizard(profile=profile)
        return action

    def load_from_file(self):
        open_dialog = QFileDialog()
        config_filename, _ = open_dialog.getOpenFileName(
            self.iface.mainWindow(),
            caption="Charger la configuration",
            filter="YAML (*.yml *.yaml)",
        )
        self.run_wizard(config_file=config_filename)


