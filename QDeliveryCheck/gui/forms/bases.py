from qgis.core import QgsCoordinateReferenceSystem
from qgis.gui import QgsFileWidget, QgsProjectionSelectionWidget
from qgis.PyQt.QtWidgets import (
    QComboBox,
    QFormLayout,
    QLabel,
    QGroupBox,
)

import mistletoe



class AbstractProfileForm:

    @classmethod
    def doc(cls):
        return "HTML user doc"

    def __init__(self):
        self._destroyed = False
        pass

    def get_layout(self):
        """ Returns a QLayout with the form widgets.
        """
        raise NotImplementedError

    def destroy(self):
        """ Destroy all the widgets contained in the layout.

        Warning: if a widget is not destroyed here it may stay
        displayed after profile changed.
        """
        self._destroyed = True

    def get_checker_args(self):
        """ Returns checkers args for the form profile
        """
        return {}


    def is_valid(self):
        if self._destroyed:
            return False
        return True


def set_srs(widget, srs):
    crs = QgsCoordinateReferenceSystem(srs)
    if crs.isValid():
        widget.setCrs(crs)


class SocleCommunBaseForm(AbstractProfileForm):

    has_gml_step = False

    checker_class = NotImplemented

    @classmethod
    def doc(cls):
        return mistletoe.markdown(cls.checker_class.generate_user_doc())

    def __init__(self, on_changed):
        super().__init__()
        self.on_changed = on_changed
        self._destroyed = False
        self.layout = QFormLayout()

        # Input and Output
        self.select_files = QgsFileWidget()
        self.select_files.fileChanged.connect(self.on_changed)
        self.select_files_label = QLabel("Fichier d'entrée")
        self.layout.addRow(self.select_files_label, self.select_files)

        self.srs = QgsProjectionSelectionWidget()
        self.srs.crsChanged.connect(self.on_changed)
        self.srs.setCrs(QgsCoordinateReferenceSystem('EPSG:3948'))
        self.layout.addRow(QLabel("SRID des données d'entrée : "), self.srs)

        self.report_dir = QgsFileWidget()
        self.report_dir.fileChanged.connect(self.on_changed)
        self.report_dir.setStorageMode(QgsFileWidget.StorageMode.GetDirectory)
        self.layout.addRow(QLabel("Dossier des sorties"), self.report_dir)

        # If and how we check bounds
        self.check_bounds = QGroupBox("Vérifier l'emprise")
        self.check_bounds.setCheckable(True)
        self.check_bounds.toggled.connect(self.on_changed)
        self.check_bounds.setChecked(False)
        check_bounds_layout = QFormLayout()

        self.bounds_file = QgsFileWidget()
        self.bounds_file.fileChanged.connect(self.on_changed)
        check_bounds_layout.addRow(QLabel("Ficher d'emprise : "), self.bounds_file)

        self.bounds_srs = QgsProjectionSelectionWidget()
        self.bounds_srs.crsChanged.connect(self.on_changed)
        self.bounds_srs.setCrs(QgsCoordinateReferenceSystem('EPSG:4326'))
        check_bounds_layout.addRow(QLabel("SRID de l'emprise : "), self.bounds_srs)

        self.check_bounds.setLayout(check_bounds_layout)
        self.layout.addRow(self.check_bounds)

        # If and how we check control points and accuracy
        # XXX tariling spaces or Qt mess with last chars ?
        self.check_control_points = QGroupBox(
            "Vérifier les aberrations planimétriques/altimétriques "
            "par rapport à des points de contrôle  "
        )
        self.check_control_points.setCheckable(True)
        self.check_control_points.toggled.connect(self.on_changed)
        self.check_control_points.setChecked(False)
        check_control_points_layout = QFormLayout()

        self.control_points_file = QgsFileWidget()
        self.control_points_file.setFilter('GPKG (*.gpkg)')
        self.control_points_file.fileChanged.connect(self.on_changed)
        check_control_points_layout.addRow(
            QLabel("Ficher de points de contrôle (Geopackage) : "),
            self.control_points_file
        )

        self.control_points_srs = QgsProjectionSelectionWidget()
        self.control_points_srs.crsChanged.connect(self.on_changed)
        self.control_points_srs.setCrs(QgsCoordinateReferenceSystem('EPSG:4326'))
        check_control_points_layout.addRow(
            QLabel("SRID des points de contrôle : "), self.control_points_srs
        )

        self.accuracy_file = QgsFileWidget()
        self.accuracy_file.setFilter('CSV (*.csv)')
        self.accuracy_file.fileChanged.connect(self.on_changed)
        check_control_points_layout.addRow(QLabel("Ficher CSV de précisions : "), self.accuracy_file)

        self.check_control_points.setLayout(check_control_points_layout)
        self.layout.addRow(self.check_control_points)

        # If and how we check DTM
        self.check_dtm = QGroupBox("Contrôler les aberrations altimétriques avec un MNT")
        self.check_dtm.setCheckable(True)
        self.check_dtm.toggled.connect(self.on_changed)
        self.check_dtm.setChecked(False)
        check_dtm_layout = QFormLayout()

        self.dtm_file = QgsFileWidget()
        self.dtm_file.setFilter('GeoTIFF (*.tif)')
        self.dtm_file.fileChanged.connect(self.on_changed)
        check_dtm_layout.addRow(QLabel("Fichier GeoTIFF du MNT : "), self.dtm_file)

        self.dtm_srs = QgsProjectionSelectionWidget()
        self.dtm_srs.crsChanged.connect(self.on_changed)
        self.dtm_srs.setCrs(QgsCoordinateReferenceSystem('EPSG:2154'))
        check_dtm_layout.addRow(QLabel("SRID du MNT : "), self.dtm_srs)

        self.dtm_accuracy_file = QgsFileWidget()
        self.dtm_accuracy_file.setFilter('CSV (*.csv)')
        self.dtm_accuracy_file.fileChanged.connect(self.on_changed)
        check_dtm_layout.addRow(QLabel("Ficher CSV de précisions : "), self.dtm_accuracy_file)

        self.check_dtm.setLayout(check_dtm_layout)
        self.layout.addRow(self.check_dtm)

    def init_from_config(self, config):
        self.select_files.setFilePath(
            config.checker.get('path', '')
        )
        set_srs(self.srs, config.checker.get('srs'))
        self.report_dir.setFilePath(config.checker.get('report_dir', ''))

        self.check_bounds.setChecked(bool(config.plugin.get('check_bounds')))
        self.bounds_file.setFilePath(config.checker.get('bounds_file_path', ''))
        set_srs(self.bounds_srs, config.checker.get('bounds_srs'))

        self.check_control_points.setChecked(bool(config.plugin.get('check_control_points')))
        self.control_points_file.setFilePath(config.checker.get('control_points_path', ''))
        set_srs(self.control_points_srs, config.checker.get('control_points_srs'))
        self.accuracy_file.setFilePath(config.checker.get('control_points_accuracy_config_path', ''))

        self.check_dtm.setChecked(bool(config.plugin.get('check_dtm')))
        self.dtm_file.setFilePath(config.checker.get('dtm_path', ''))
        set_srs(self.dtm_srs, config.checker.get('dtm_srs'))
        self.dtm_accuracy_file.setFilePath(config.checker.get('dtm_accuracy_config_path'))

    def get_layout(self):
        return self.layout

    def destroy(self):
        super().destroy()
        self.layout.removeRow(self.select_files)
        self.layout.removeRow(self.srs)
        self.layout.removeRow(self.report_dir)
        self.layout.removeRow(self.check_bounds)
        self.layout.removeRow(self.check_control_points)
        self.layout.removeRow(self.check_dtm)

    def is_valid(self):
        if not super().is_valid():
            return False
        if not self.select_files.filePath():
            return False
        if not self.srs.crs().authid():
            return False
        if self.check_bounds.isChecked():
            if not self.bounds_file.filePath():
                return False
            if not self.bounds_srs.crs().authid():
                return False
        if self.check_control_points.isChecked():
            if not self.control_points_file.filePath():
                return False
            if not self.control_points_srs.crs().authid():
                return False
            if not self.accuracy_file.filePath():
                return False
        if self.check_dtm.isChecked():
            if not self.dtm_file.filePath():
                return False
            if not self.dtm_srs.crs().authid():
                return False
            if not self.dtm_accuracy_file.filePath():
                return False
        if not self.report_dir.filePath():
            return False
        return True

    def validate_form(self, config):
        config.plugin.update({
            'check_bounds': self.check_bounds.isChecked(),
            'check_control_points': self.check_control_points.isChecked(),
            'check_dtm': self.check_dtm.isChecked(),
        })
        config.checker.update({
            'path': self.select_files.filePath(),
            'srs': self.srs.crs().authid(),
            'report_dir': self.report_dir.filePath(),
            'bounds_file_path': self.bounds_file.filePath(),
            'bounds_srs': self.bounds_srs.crs().authid(),
            'control_points_path': self.control_points_file.filePath(),
            'control_points_srs': self.control_points_srs.crs().authid(),
            'control_points_accuracy_config_path': self.accuracy_file.filePath(),
            'dtm_path': self.dtm_file.filePath() if self.check_dtm.isChecked() else None,
            'dtm_srs': self.dtm_srs.crs().authid(),
            'dtm_accuracy_config_path': self.dtm_accuracy_file.filePath(),
        })

    def get_checker_args(self, config):
        args = {
            'path': config.checker['path'],
            'srs': config.checker['srs'],
            'report_dir': config.checker['report_dir'],
            'bounds_file_path': (
                config.checker['bounds_file_path'] if config.plugin['check_bounds'] else None
            ),
            'bounds_srs': config.checker['bounds_srs'],
            'control_points_path': (
                config.checker['control_points_path'] if config.plugin['check_control_points']
                else None
            ),
            'control_points_accuracy_config_path': (
                config.checker['control_points_accuracy_config_path']
                if config.plugin['check_control_points'] else None
            ),
            'dtm_path': (
                config.checker['dtm_path'] if config.plugin['check_dtm'] else None
            ),
            'dtm_srs': config.checker['dtm_srs'],
            'dtm_accuracy_config_path': (
                config.checker['dtm_accuracy_config_path'] if config.plugin['check_dtm'] else None
            ),
        }
        if self.has_gml_step and config.plugin['export_gml']:
            args['gml_export_options'] = config.checker['gml_export_options']
        return args


