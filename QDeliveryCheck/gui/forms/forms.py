from qgis.core import QgsCoordinateReferenceSystem
from qgis.gui import QgsFileWidget
from qgis.PyQt.QtWidgets import (
    QComboBox,
    QFormLayout,
    QLabel,
    QGroupBox,
    QCheckBox,
)

from QDeliveryCheck.profiles import Profiles

from deliverycheck.checks.ref_topo_geobzh.commun import SocleCommunDXFCheck
from deliverycheck.checks.ref_topo_geobzh.sdef import SDEFShapefileCheck
from deliverycheck.checks.ref_topo_geobzh.sdef.shapefile import VectorDataType
from deliverycheck.checks.ref_topo_geobzh.lorient import LorientAgglomerationDXFCheck
from deliverycheck.checks.ref_topo_geobzh.rennes import RennesMetropoleDXFCheck

from .bases import SocleCommunBaseForm
from .registry import forms_registry


@forms_registry.register(Profiles.SOCLE_COMMUN_DXF)
class SocleCommunDXFForm(SocleCommunBaseForm):

    checker_class = SocleCommunDXFCheck

    has_gml_step = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.select_files.setStorageMode(QgsFileWidget.StorageMode.GetFile)
        self.select_files_label.setText("Fichier d'entrée")
        self.select_files.setFilter('DXF (*.dxf)')


@forms_registry.register(Profiles.SDEF_SHP)
class SDEFSHPForm(SocleCommunBaseForm):

    checker_class = SDEFShapefileCheck

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.select_files_label.setText("Dossier d'entrée")
        self.select_files.setStorageMode(QgsFileWidget.StorageMode.GetDirectory)

        self.data_type = QComboBox()
        self.data_type.addItem(VectorDataType.RTS, VectorDataType.RTS)
        self.data_type.addItem(VectorDataType.RTGE, VectorDataType.RTGE)
        self.layout.insertRow(2, QLabel("Type de donnée"), self.data_type)

        self.remove_small_geoms = QCheckBox(
            "Nettoyer les petites géométries pour l'export GML"
        )
        self.layout.addRow(self.remove_small_geoms)

    def init_from_config(self, config):
        super().init_from_config(config)
        self.data_type.setCurrentText(config.checker.get('data_type'))
        self.remove_small_geoms.setChecked(config.checker.get('remove_small_geometries', False))

    def destroy(self):
        super().destroy()
        self.layout.removeRow(self.data_type)
        self.layout.removeRow(self.remove_small_geoms)

    def validate_form(self, config):
        super().validate_form(config)
        config.checker['data_type'] = self.data_type.currentData()
        config.checker['remove_small_geometries'] = self.remove_small_geoms.isChecked()

    def get_checker_args(self, config):
        return dict(
            **super().get_checker_args(config),
            data_type=config.checker['data_type'],
            remove_small_geometries=config.checker['remove_small_geometries'],
        )


@forms_registry.register(Profiles.LORIENT_AGGLO_DXF)
class LorientAggloDXFForm(SocleCommunDXFForm):

    checker_class = LorientAgglomerationDXFCheck

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.check_archive = QGroupBox("Vérifier les fournitures de la prestation")
        self.check_archive.setCheckable(True)
        self.check_archive.toggled.connect(self.on_changed)
        self.check_archive.setChecked(False)
        check_archive_layout = QFormLayout()

        self.archive_file = QgsFileWidget()
        self.archive_file.setFilter('ZIP (*.zip)')
        self.archive_file.fileChanged.connect(self.on_changed)
        check_archive_layout.addRow(QLabel("Ficher ZIP : "), self.archive_file)

        self.check_archive.setLayout(check_archive_layout)
        self.layout.addRow(self.check_archive)

    def init_from_config(self, config):
        super().init_from_config(config)
        self.check_archive.setChecked(bool(config.plugin.get('check_archive')))
        self.archive_file.setFilePath(config.checker.get('archive_path', ''))

    def destroy(self):
        super().destroy()
        self.layout.removeRow(self.check_archive)

    def is_valid(self):
        if not super().is_valid():
            return False
        if self.check_archive.isChecked():
            if not self.archive_file.filePath():
                return False
        return True

    def validate_form(self, config):
        super().validate_form(config)
        config.plugin['check_archive'] = self.check_archive.isChecked()
        config.checker['archive_path'] = self.archive_file.filePath()

    def get_checker_args(self, config):
        return dict(
            **super().get_checker_args(config),
            archive_path=config.checker['archive_path'] if config.plugin['check_archive'] else None,
        )


@forms_registry.register(Profiles.RENNES_METRO_DXF)
class RennesMetropoleDXFForm(SocleCommunDXFForm):

    checker_class = RennesMetropoleDXFCheck

