from .registry import forms_registry

# import here all forms modules
# to ensure registration is done
from .forms import *
