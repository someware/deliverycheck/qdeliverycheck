

class FormsRegistry:

    def __init__(self):
        self._registry = {}

    def register(self, profile):
        def decorator(form_class):
            self._registry[profile] = form_class
            return form_class
        return decorator

    def __getitem__(self, key):
        return self._registry[key]

    def get(self, key, default=None):
        return self._registry.get(key, default)


forms_registry = FormsRegistry()

