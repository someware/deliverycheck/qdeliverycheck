import html
import os
import traceback

from qgis.core import Qgis, QgsMessageLog, QgsProject, QgsVectorLayer
from qgis.core import QgsTask, QgsApplication

from qgis.PyQt.QtCore import Qt, QUrl, pyqtSignal
from qgis.PyQt.QtGui import QDesktopServices, QColor
from qgis.PyQt.QtWidgets import (
    QFileDialog,
    QWizardPage,
    QLabel,
    QVBoxLayout,
    QHBoxLayout,
    QPushButton,
    QProgressBar,
)

from QDeliveryCheck.profiles import Profiles
from .widgets import OpenGpkgWidget

from osgeo import ogr

GEOPACKAGES_GEOM_TYPES = [
    'Point',
    'LineString',
    'Polygon',
]


TEXT_SELECTABLE = (
    Qt.TextInteractionFlag.TextSelectableByMouse |
    Qt.TextInteractionFlag.TextSelectableByKeyboard
)



class Runner(QgsTask):

    execution_count = pyqtSignal(int, int, name="executionCount")

    task_crashed = pyqtSignal(Exception, name="unexpectedError")

    def __init__(self, description, checker):
        super().__init__(description)
        self.checker = checker

    def canCancel(self):
        return True

    def normalize_progress(self, current, max_):
        return int((current / max_) * 100)

    def execute(self):
        checks_count = self.checker.count_checks()
        for i, _ in enumerate(self.checker.generate_checks(), start=1):
            self.execution_count.emit(i, checks_count)
            self.setProgress(self.normalize_progress(i, checks_count))
            if self.isCanceled():
                return False
        self.checker.write_check_reports()
        self.setProgress(100)
        return True

    def run(self):
        QgsMessageLog.logMessage('start runner', 'QDeliveryCheck', level=Qgis.Info)
        try:
            is_complete = self.execute()
            if is_complete:
                QgsMessageLog.logMessage('runner finished', 'QDeliveryCheck', level=Qgis.Info)
            else:
                QgsMessageLog.logMessage('runner canceled', 'QDeliveryCheck', level=Qgis.Info)
        except Exception as e:
            QgsMessageLog.logMessage('runner crashed', 'QDeliveryCheck', level=Qgis.Critical)
            QgsMessageLog.logMessage(
                html.escape(traceback.format_exc()),
                'QDeliveryCheck',
                level=Qgis.Critical,
            )
            is_complete = False
            self.task_crashed.emit(e)

        return is_complete



class ExecutePage(QWizardPage):

    def __init__(self, wizard):
        self.wizard = wizard
        super().__init__()

        self.has_ran = False
        self.task = None

        main_layout = QVBoxLayout()

        self.open_conversion = OpenGpkgWidget(
            label="Afficher les couches d'entrée dans QGIS",
            default_group_name="Conversion QDeliveryCheck",
        )
        main_layout.addWidget(self.open_conversion)

        self.open_report = OpenGpkgWidget(
            label="Afficher les couches d'erreur dans QGIS",
            default_group_name="Rapport QDeliveryCheck",
        )
        main_layout.addWidget(self.open_report)


        buttons_layout = QHBoxLayout()

        self.exec_button = QPushButton("Exécuter")
        self.exec_button.clicked.connect(self.execute)
        buttons_layout.addWidget(self.exec_button)

        self.save_button = QPushButton("Enregistrer la configuration")
        self.save_button.clicked.connect(self.save_config)
        buttons_layout.addWidget(self.save_button)

        self.open_report_dir = QPushButton("Ouvrir le dossier de sorties")
        self.open_report_dir.setEnabled(False)
        buttons_layout.addWidget(self.open_report_dir)

        main_layout.addLayout(buttons_layout)

        self.progress_bar = QProgressBar()
        self.progress_bar.setTextVisible(False)
        self.progress_bar.hide()
        main_layout.addWidget(self.progress_bar)

        self.reading_inputs = QLabel()
        self.reading_inputs.setTextInteractionFlags(TEXT_SELECTABLE)
        main_layout.addWidget(self.reading_inputs)

        self.executed_checks = QLabel()
        self.executed_checks.setTextInteractionFlags(TEXT_SELECTABLE)
        main_layout.addWidget(self.executed_checks)

        self.writing_outputs = QLabel()
        self.writing_outputs.setTextInteractionFlags(TEXT_SELECTABLE)
        main_layout.addWidget(self.writing_outputs)

        self.logs = QLabel()
        self.logs.setWordWrap(True)
        self.logs.setTextInteractionFlags(TEXT_SELECTABLE)
        main_layout.addWidget(self.logs)

        self.setLayout(main_layout)

        self.init_from_config()

    def set_has_ran(self, value):
        self.has_ran = value
        self.completeChanged.emit()

    def initializePage(self):
        self.set_has_ran(False)
        self.setTitle("Exécution de %s" % Profiles[self.wizard.config.plugin['profile']].value)

    def init_from_config(self):
        self.open_conversion.set_value(self.wizard.config.plugin.get('open_conversion', {}))
        self.open_report.set_value(self.wizard.config.plugin.get('open_report', {}))

    def validate_page(self):
        self.wizard.config.plugin['open_conversion'] = self.open_conversion.get_value()
        self.wizard.config.plugin['open_report'] = self.open_report.get_value()

    def save_config(self):
        self.validate_page()
        save_dialog = QFileDialog(
            self,
            "Sauvegarder la configuration",
            "deliverycheck_config.yml",
            "YAML (*.yml *.yaml)"
        )
        save_dialog.setAcceptMode(QFileDialog.AcceptSave)
        save_dialog.setDefaultSuffix('yml')
        if save_dialog.exec():
            config_filename = save_dialog.selectedFiles()[0]
            self.wizard.dump_config(config_filename)

    def execute(self):
        self.validate_page()
        self.exec_button.setEnabled(False)
        self.open_report_dir.setEnabled(False)
        self.progress_bar.show()
        self.progress_bar.setRange(0, 0)
        self.reading_inputs.setText('')
        self.executed_checks.setText('')
        self.writing_outputs.setText('')
        self.logs.setText('')

        checker = self.wizard.get_checker()
        self.connect_open_report_dir(checker)

        self.task = Runner('QDeliveryCheck', checker=checker)
        self.task.execution_count.connect(self.show_progress)
        self.task.task_crashed.connect(self.on_crash)
        self.task.taskTerminated.connect(self.on_terminated)
        self.task.taskCompleted.connect(self.make_on_finished(checker))

        self.reading_inputs.setText("Lecture des données...")
        QgsApplication.taskManager().addTask(self.task)

    def show_progress(self, current, count):
        self.executed_checks.setText("Contrôles exécutés : %s/%s" % (current, count))
        self.progress_bar.setMaximum(count)
        self.progress_bar.setValue(current)
        if current >= count:
            self.writing_outputs.setText("Écriture des rapports...")
            self.progress_bar.setRange(0, 0)

    def on_crash(self, exception):
        self.logs.setText('\n'.join([
            "Erreur d'exécution",
            str(exception),
        ]))

    def make_on_finished(self, checker):
        def task():
            self.progress_bar.hide()
            self.exec_button.setEnabled(True)
            self.open_report_dir.setEnabled(True)
            self.show_report(checker)
            self.wizard.iface.messageBar().pushMessage("QDeliveryCheck", "Run finished", level=Qgis.Info)
            self.set_has_ran(True)
            self.task = None
            self.completeChanged.emit()
        return task

    def on_terminated(self):
        self.progress_bar.hide()
        self.writing_outputs.setText("Exécution interrompue")
        self.exec_button.setEnabled(True)
        self.wizard.iface.messageBar().pushMessage("QDeliveryCheck", "Run aborted", level=Qgis.Warning)
        self.set_has_ran(True)
        self.task = None
        self.completeChanged.emit()


    def isComplete(self):
        return self.has_ran

    def cancel(self):
        if self.task:
            self.task.cancel()
            self.task = None

    def show_report(self, checker):
        messages = checker.check_logs
        errors_count = len([m for m in messages if m.criticity == 'ERROR'])
        warnings_count = len([m for m in messages if m.criticity == 'WARNING'])
        report_text = '\n'.join([
            "Exécution terminée",
            "  - %s avertissement(s)" % warnings_count,
            "  - %s erreur(s)" % errors_count,
        ])
        self.logs.setText(report_text)

        if self.open_conversion.shouldOpen():
            open_geopackage(
                os.path.join(checker.report_dir, 'conversion.gpkg'),
                self.open_conversion.groupName(),
                color=QColor('black'),
            )

        if self.open_report.shouldOpen():
            open_geopackage(
                os.path.join(checker.report_dir, 'rapport.gpkg'),
                self.open_report.groupName(),
                color=QColor('red'),
            )

    def connect_open_report_dir(self, checker):
        def show_report_dir(self):
            QDesktopServices.openUrl(QUrl.fromLocalFile(checker.report_dir))
        try:
            self.open_report_dir.clicked.disconnect()
        except TypeError:
            # no handler connected to click signal
            pass
        self.open_report_dir.clicked.connect(show_report_dir)


def list_layers_names(path):
    conn = ogr.Open(path)
    if conn:
        for layer in conn:
            yield layer.GetName()
    yield from ()


def open_geopackage(file_path, group_name, tree_position=0, color=None):
    group = QgsProject.instance().layerTreeRoot().insertGroup(tree_position, group_name)
    for layer_name in list_layers_names(file_path):
        for geom_type in GEOPACKAGES_GEOM_TYPES:
            vlayer = QgsVectorLayer(
                '%s|layername=%s|geometrytype=%s' % (file_path, layer_name, geom_type),
                layer_name,
                'ogr',
            )
            if color:
                vlayer.renderer().symbol().setColor(color)
            if vlayer.featureCount():
                layer = QgsProject.instance().addMapLayer(vlayer, False)
                group.addLayer(layer)
