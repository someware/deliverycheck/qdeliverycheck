import webbrowser
import yaml

from qgis.PyQt.QtWidgets import QWizard

from QDeliveryCheck import __help__
from .select_profile_page import SelectedProfilePage
from .config_page import ConfigPage
from .gml_pages import GMLPage1, GMLPage2
from .execute_page import ExecutePage



class WizardConfig:

    def __init__(self):
        self.plugin = {}
        self.checker = {}

    def load(self, stream):
        raw_config = yaml.load(stream, Loader=yaml.SafeLoader)
        self.plugin = raw_config.get('plugin', {})
        self.checker = raw_config.get('checker', {})

    def dump(self, stream):
        yaml.dump(
            dict(plugin=self.plugin, checker=self.checker),
            stream,
            Dumper=yaml.SafeDumper
        )



class PluginWizard(QWizard):

    def __init__(self, iface, profile=None, config_file=None):
        self.iface = iface
        self.config_file=config_file
        self.form = None
        super().__init__()
        self.setWindowTitle("QDeliveryCheck")
        self.setOption(QWizard.HaveHelpButton, True)
        self.helpRequested.connect(self.on_help)

        self.setOption(QWizard.HaveCustomButton1, True)
        self.setButtonText(QWizard.CustomButton1, "About");

        self.config = WizardConfig()
        self.load_config()

        # wizard pages
        self.select_profile_page = SelectedProfilePage(self)
        self.addPage(self.select_profile_page)
        self.config_page = ConfigPage(self)
        self.addPage(self.config_page)
        self.gml_page_1 = GMLPage1(self)
        self.addPage(self.gml_page_1)
        self.gml_page_2 = GMLPage2(self)
        self.addPage(self.gml_page_2)
        self.execute_page = ExecutePage(self)
        self.addPage(self.execute_page)

        # try to cancel any run when wizard is canceled
        cancel_button = self.button(QWizard.CancelButton)
        cancel_button.clicked.connect(self.execute_page.cancel)

        if profile:
            self.start(profile)

    def start(self, profile=None):
        self.config.plugin['profile'] = profile.name
        self.restart()
        if self.config.plugin.get('profile') is not None:
            self.next()

    def get_checker_args(self):
        return self.form.get_checker_args(self.config)

    def get_checker(self):
        if self.form:
            return self.form.checker_class(**self.get_checker_args())

    def dump_config(self, filename):
        with open(filename, 'w') as f:
            self.config.dump(f)

    def load_config(self):
        if self.config_file:
            with open(self.config_file, 'r') as f:
                self.config.load(f)

    def on_help(self):
        webbrowser.open(__help__)

