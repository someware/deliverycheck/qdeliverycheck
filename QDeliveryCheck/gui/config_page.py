from qgis.PyQt.QtCore import pyqtSignal
from qgis.PyQt.QtWidgets import QWizardPage, QVBoxLayout

from QDeliveryCheck.profiles import Profiles
from .forms import forms_registry


class ConfigPage(QWizardPage):

    def __init__(self, wizard):
        super().__init__()
        self.form = None
        self.wizard = wizard

        main_layout = QVBoxLayout()
        self.setLayout(main_layout)

        self.initialize_profile(self.wizard.config.plugin.get('profile'))

    def on_form_changed(self):
        self.completeChanged.emit()

    def isComplete(self):
        if not self.form:
            return False
        return self.form.is_valid()

    def initializePage(self):
        if self.profile != self.wizard.config.plugin['profile']:
            self.initialize_profile(self.wizard.config.plugin['profile'])

    def initialize_profile(self, profile):
        self.profile = Profiles[profile] if profile else None
        # clean
        self.setTitle("")
        if self.form:
            self.form.destroy()
        if self.profile:
            # display profile
            self.setTitle("Configuration de %s" % self.profile.value)
            form = forms_registry[self.profile](
                self.on_form_changed
            )
            form.init_from_config(self.wizard.config)
            self.form = form
            self.layout().addLayout(self.form.get_layout())

    def validatePage(self):
        self.wizard.form = self.form
        self.form.validate_form(self.wizard.config)
        return True

    def nextId(self):
        current = self.wizard.currentId()
        has_gml_step = self.form.has_gml_step if self.form else False
        return current + 1 if has_gml_step else current + 3

