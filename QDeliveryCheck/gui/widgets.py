from qgis.PyQt.QtWidgets import (
    QWidget,
    QFormLayout,
    QCheckBox,
    QLabel,
    QLineEdit,
)


class OpenGpkgWidget(QWidget):

    def __init__(self, label, default_group_name, default_should_open=True):
        super().__init__()
        self.layout = QFormLayout()
        self.should_open = QCheckBox(label)
        self.should_open.stateChanged.connect(self.on_should_open_changed)

        self.group_label = QLabel("Nom du groupe")
        self.group_name = QLineEdit(default_group_name)

        self.layout.addRow(self.should_open)
        self.should_open.setChecked(default_should_open)

        self.layout.addRow(self.group_label, self.group_name)

        self.setLayout(self.layout)

    def on_should_open_changed(self, value):
        self.group_name.setEnabled(value)

    def shouldOpen(self):
        return self.should_open.isChecked()

    def groupName(self):
        return self.group_name.text().strip()

    def get_value(self):
        return {
            'should_open': self.shouldOpen(),
            'group_name': self.groupName(),
        }

    def set_value(self, value):
        self.should_open.setChecked(bool(value.get('should_open', True)))
        if value.get('group_name'):
            self.group_name.setText(value['group_name'])


