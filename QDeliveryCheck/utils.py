import os
import sys

from contextlib import contextmanager


_HERE = os.path.dirname(__file__)
_LIB_PATH = os.path.join(_HERE, 'lib')
_WHEEL_PATH = os.path.join(_HERE, 'wheels')

LIBS = [
    os.path.join(_WHEEL_PATH, 'ezdxf-1.1.0-py3-none-any.whl'),
    os.path.join(_WHEEL_PATH, 'pyparsing-3.0.9-py3-none-any.whl'),
    os.path.join(_WHEEL_PATH, 'typing_extensions-4.3.0-py3-none-any.whl'),
    os.path.join(_WHEEL_PATH, 'Cerberus-1.3.4-py3-none-any.whl'),
    os.path.join(_WHEEL_PATH, 'mistletoe-0.9.0-py3-none-any.whl'),
    os.path.join(_WHEEL_PATH, 'elementpath-4.4.0-py3-none-any.whl'),
    os.path.join(_WHEEL_PATH, 'xmlschema-3.3.1-py3-none-any.whl_FILES'),
    os.path.join(_WHEEL_PATH, 'PyYAML-5.4.1-py3-none-any.whl'),
    os.path.join(_WHEEL_PATH, 'fonttools-4.42.1-py3-none-any.whl'),
    _LIB_PATH,
]


@contextmanager
def external_libs():
    for path in LIBS:
        sys.path.append(path)
    try:
        yield
    finally:
        for path in LIBS:
            sys.path.remove(path)

