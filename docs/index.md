# QDeliveryCheck {{ downloads.version }}

Bienvenue sur la documentation de QDeliveryCheck !

QDeliveryCheck est un plugin pour [QGIS](https://www.qgis.org/) permettant le contrôle de données topographiques et l'export de celles-ci au format PCRS (GML).

## Fonctionnement

Le fonctionnement du plugin est le suivant :

* Il est d'abord nécessaire de sélectionner un profil de contrôle (voir la liste des profils ci-dessous)
* Il faut ensuite renseigner les paramètres attendus par le profil choisi, certains paramètres étant optionnels. Parmi les paramètres attendus, il y a bien sûr le fichier à contrôler, mais aussi le dossier de sortie pour le rapport de contrôle et les différentes données écrites par le plugin.
* Enfin, il faut lancer l'exécution des contrôles
* Le plugin va ensuite générer un rapport au format XLSX détaillant les opérations réalisées, les objets sur lesquels une erreur (bloquant) ou warning (non bloquant) a été détectée. Une conversion au format GeoPackage des données en entrée est aussi réalisée.
* A l'issue des contrôles, si aucune erreur a été trouvée, le plugin va convertir les données au format PCRS GML.


## Profils de contrôle

Le plugin supporte 4 profils pour les données:

* Le profil **[Format DXF](checks/commun_dxf.md)**, correspondant au Standard 
  topographique régional
* Le profil **[Lorient Agglomération (DXF)](checks/lo_dxf.md)**, avec des 
  objets et contrôles en plus de ceux du standard
* Le profil **[Rennes Métropole (DXF)](checks/rm_dxf.md)**, avec des objets et 
  contrôles en plus de ceux du standard
* Le profil **[SDEF (Shapefile)](checks/sdef_shp.md)** avec deux modèles de 
  données SIG possibles (RTS et RTGE), reprenant les concepts du standard


## Installation

### Prérequis

Ce plugin nécessite [QGIS](https://www.qgis.org/) dans sa version 3.


### Utiliser le dépôt QGIS QDeliveryCheck

Configurer un dépôt QGIS avec l'url suivante : 

```text
{{ downloads.repository }}
```

Pour cela, ouvrir la fenêtre Extensions > Installer/Gérer les extensions.

Dans l'onglet Paramètres, section Dépôts d'extensions, renseigner l'adresser ci-dessus en cliquant sur Ajouter.

Le plugin sera ainsi disponible à l'installation (onglet Toutes de la fenêtre Installer/Gérer les extensions), et QGIS se chargera de vérifier automatiquement si des mises à jour sont disponibles.

### Installer manuellement le paquet

Télécharger la dernière version : <{{ downloads.package }}>

Ouvrir la fenêtre Extensions > Installer/Gérer les extensions.

Dans l'onglet Installer depuis un ZIP, choisir le fichier téléchargé, puis cliquer sur Installer le plugin.

Nous vous recommandons de fermer et relancer QGIS pour le bon fonctionnement de l'installation.


## Licence

Copyright © 2023 [Someware](https://www.someware.fr)

QDeliveryCheck est distribué sous licence open source [GPL V3](LICENSE.md).

Son module DeliveryCheck est distribué sous licence open source CeCILL-C.

## Crédits

Le développement initial de QDeliveryCheck a été porté par les organismes suivants:

* Région Bretagne
* Région Pays de Loire
* Lorient Agglomération
* Rennes Métropole
* Brest Métropole
* Lannion-Trégor Communauté
* SDEF
* SDE 22
* SDE 35
* Morbihan Énergies
* Redon Agglomération
* Quimperlé Communauté
* Ploërmel Communauté
* Communauté de Communes du Pays Fouesnantais

Les développements ont été réalisés par la société [Someware](https://www.someware.fr)
