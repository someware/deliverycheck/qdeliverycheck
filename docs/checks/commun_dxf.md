# Profil Format DXF

Le profil Format DXF permet de vérifier la conformité de livrables au format 
DXF selon le modèle topographique GeoBretagne.

{% include-markdown "./commun_dxf_parameters.md" %}

## Listing des contrôles du profil

// {{ checker_doc('deliverycheck.checks.ref_topo_geobzh.commun.dxf', 'SocleCommunDXFCheck') }}
