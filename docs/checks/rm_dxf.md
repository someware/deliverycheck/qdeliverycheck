# Profil Rennes Métropole (DXF)

Le profil Rennes Métropole (DXF) permet de vérifier la conformité de livrables 
au format DXF selon une variante du modèle topographique GeoBretagne propre aux 
besoins de Rennes Métropole. Ce profil comporte par ailleurs quelques contrôles 
en complément de ceux du profil [Format DXF](commun_dxf.md).


{% include-markdown "./commun_dxf_parameters.md" %}


## Listing des contrôles du profil

{{ checker_doc('deliverycheck.checks.ref_topo_geobzh.rennes.dxf', 'RennesMetropoleDXFCheck') }}

