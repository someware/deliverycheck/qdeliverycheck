# Profil Lorient Agglomération (DXF)

Le profil Lorient Agglomération (DXF) permet de vérifier la conformité de 
livrables au format DXF selon une variante du modèle topographique GeoBretagne 
propre aux besoins de Lorient Agglomération. Ce profil comporte par ailleurs 
quelques contrôles en complément de ceux du profil [Format DXF](commun_dxf.md).

{% include-markdown "./commun_dxf_parameters.md" %}


### Vérifier les fournitures de la prestation


Ce contrôle permet de vérifier que le fichier ZIP livré est structuré de la manière attendue par Lorient Agglomération.

Ses paramètres sont les suivants:

- **Ficher ZIP**: fichier ZIP


## Listing des contrôles du profil

{{ checker_doc('deliverycheck.checks.ref_topo_geobzh.lorient.dxf',  'LorientAgglomerationDXFCheck') }}
