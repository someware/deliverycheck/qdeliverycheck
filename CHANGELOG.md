# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## 1.0.10 - 2024-09-10

- Upgrade lib to 1.0.17
- Fix to handle null geometries in SDEF Shapefile profile

## 1.0.9 - 2024-09-02

- Upgrade elementpath to 4.4.0
- Fix profiles names in docs

## 1.0.8 - 2024-08-23

- Upgradde deliverycheck to 1.0.16
- All DXF check profiles : fix check OBJETS_NON_AUTORISES so that socle_continu 
  and socle_discontinu can be used ONLY for entities of type INSERT or HATCH
- change all profiles user_doc to Standard topographique régional...
- improve computation of an INSERT text (by calling attrib TYPE if TXT is not set)
- add new param prefixe_id for method EXPORT_GML in all DXF profiles, that can be applied when computing the pcrs:idHabillage to create a unique value for later DB import
- update version of xmlschema
- rename profiles in UI and docs
- upgrade dev dependencies
- upgrade xmlschema wheel to 3.3.1
- test plugin with QGIS 3.34 on Windows

## 1.0.7 - 2024-02-01

- Upgrade deliverycheck to 1.0.15
- fix ALT_MALT check to return warnings for altitudes <= 0
- improve check GEOMETRIES_NON_AUTORISEES to return warnings when object 
  geometries contain arcs
- convert arcs (in LWPolylines) as GIS linestrings
- improve length of GML coordinates
- fix check_27_dtm by calling a method that reads 3D geoms properly (points were read as 2D)
- SDEF profile : add new check to check validity, length and area of geometries
- SDEF profile : add new sdef profile setting (remove_small_geometries : 
  Boolean) to automatically remove small geometries in the GML export (in this 
  case, only warnings are returned for such geometries in 
  check_valid_geometries)
- SDEF profile :  add new check check_pile_pont_geometries to return errors 
  when PilePont linestrings have less than 3 points (cannot be exported to GML)
- Add GUI option to remove small geometries during GML export in SDEF profile

## 1.0.6 - 2023-11-21

- Upgrade deliverycheck to 1.0.12
- Change ALTITUDE check for DXF profiles so that warnings (instead of errors)
  are raised for altitudes that are <= 0

## 1.0.5 - 2023-11-10

- Upgrade deliverycheck to 1.0.11
- improve SDEF profile by changing altitude_z definitioN. Now it must be a
  float (or a string that can be coerced as a float), with no check on the
  minimum value

## 1.0.4 - 2023-10-18

- fix DTM file path in config
- fix about dialog content

## 1.0.3 - 2023-09-26

- Upgrade deliverycheck to 1.0.10
- upgrade ezdxf to fix an issue computing extent of some dxf entities

## 1.0.2 - 2023-09-21

- Upgrade deliverycheck to 1.0.9 (!2)
    - fix SDEF GML export to be compliant with IGN PCRS Validator
    - fix GIS conversion of DXF HATCH entities, by bypassing ARC/SPLINE 
      sub-entities

## 1.0.1 - 2023-06-30

- Upgrade deliverycheck to 1.0.8 (!1):
    - improve SDEF check profile and tests

## 1.0.0 - 2023-06-22

- First stable version


## 1.0.0-beta1 - 2023-06-22

- Share the code with the world
