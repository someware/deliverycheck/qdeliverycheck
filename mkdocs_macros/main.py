import importlib

def define_env(env):

    @env.macro
    def checker_doc(module, name):
        module = importlib.import_module(module)
        checker = getattr(module, name)
        return checker.generate_user_doc(first_level=2, add_profile_name=False)


